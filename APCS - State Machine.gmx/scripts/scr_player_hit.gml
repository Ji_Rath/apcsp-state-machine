///scr_player_hit();

//Lose 1 hp on initial hit
if image_speed = 0 {hp -= 1;}

//Play animation
image_speed = 0.15*global.delta

//Temp Variables
var runmult = 1.5;

//Movement code
if keyboard_check(ord("W")) {
    y -= spd*runmult*global.delta;
}
if keyboard_check(ord("A")) {
    x -= spd*runmult*global.delta;
}
if keyboard_check(ord("S")) {
    y += spd*runmult*global.delta;
}
if keyboard_check(ord("D")) {
    x += spd*runmult*global.delta;
}

if image_index > 2 {image_speed = 0; image_index = 0; state = states.normal;}

if hp < 1 {
    state = states.dead;
}
