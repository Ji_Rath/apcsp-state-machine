///scr_player_shooting();

//Create Bullet
if (canshoot) {
    instance_create(x,y,obj_bullet);
    canshoot = false;
    timer[0] = 1*60;
}

//Point player at mouse
image_angle = point_direction(x,y,mouse_x,mouse_y);

if !mouse_check_button(mb_left) {
    state = states.normal;
}

if (x !=xprevious || y != yprevious) && keyboard_check(vk_lshift) {
    state = states.running;
}

//If hit by an enemy
if place_meeting(x,y,obj_enemy) {
    hp -= 1;
    state = states.hit;
}
