///scr_player_running();

//Temp Variables
var runmult = 1.25;

//Movement code
if keyboard_check(ord("W")) {
    y -= spd*runmult*global.delta;
    image_angle = 90;
}
if keyboard_check(ord("A")) {
    x -= spd*runmult*global.delta;
    image_angle = 180;
}
if keyboard_check(ord("S")) {
    y += spd*runmult*global.delta;
    image_angle = 270;
}
if keyboard_check(ord("D")) {
    x += spd*runmult*global.delta;
    image_angle = 0;
}

//Stop sprinting
if !keyboard_check(vk_lshift) {
    state = states.normal;
}
