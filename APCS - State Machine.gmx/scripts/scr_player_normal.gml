///scr_player_normal()

//Movement code
if keyboard_check(ord("W")) {
    y -= spd*global.delta;
}
if keyboard_check(ord("A")) {
    x -= spd*global.delta;
}
if keyboard_check(ord("S")) {
    y += spd*global.delta;
}
if keyboard_check(ord("D")) {
    x += spd*global.delta;
}

//Point at mouse
image_angle = point_direction(x,y,mouse_x,mouse_y);

//If sprinting
if keyboard_check(vk_lshift) {
    state = states.running;
}

//If hit by an enemy
if place_meeting(x,y,obj_enemy) {
    hp -= 1;
    state = states.hit;
}

//If shooting
if mouse_check_button(mb_left) {
    state = states.shooting;
}


